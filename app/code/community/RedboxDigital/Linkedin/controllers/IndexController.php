<?php
/**
 * Created by PhpStorm.
 * User: Vanessa Boodoo
 * Date: 10/23/14
 * Time: 9:40 AM
 */

class RedboxDigital_Linkedin_IndexController
    extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->getResponse()->setBody('RedboxDigital_Linkedin');
    }
}