<?php
/**
 * Created by PhpStorm.
 * User: Vanessa Boodoo
 * Date: 10/21/14
 * Time: 9:51 PM
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId = $setup->getEntityTypeId('customer');

$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);

$setup->addAttribute($entityTypeId, 'linkedin_profile', array(
    'input'                     => 'text',
    'type'                      => 'varchar',
    'frontend'                  => '',
    'label'                     => 'Linkedin Profile',
    'user_defined'              => 0,
    'filterable'                => false,
    'comparable'                => false,
    'visible_on_front'          => false,
    'required'                  => 0,
    'is_filterable'             => 0,
    'unique'                    => false,
));

$newAttribute = Mage::getSingleton('eav/config')->getAttribute($entityTypeId, 'linkedin_profile');
$newAttribute->setData('used_in_forms', array('adminhtml_customer', 'customer_account_create','customer_account_edit'));
$newAttribute->save();

$installer->endSetup();