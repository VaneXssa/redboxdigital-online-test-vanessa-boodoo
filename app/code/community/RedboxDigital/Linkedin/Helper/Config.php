<?php
/**
 * Created by PhpStorm.
 * User: Vanessa Boodoo
 * Date: 10/23/14
 * Time: 10:57 AM
 */
class RedboxDigital_Linkedin_Helper_Config
    extends Mage_Core_Helper_Abstract
{
    CONST REDBOX_DIGITAL_LINKEDIN_IS_REQUIRED = 'redboxdigital_linkedin/Linkedin/isRequired';
    CONST REDBOX_DIGITAL_LINKEDIN_MAXLENGTH = 'redboxdigital_linkedin/Linkedin/maxLength';

    public function getIsRequired()
    {
        return Mage::getStoreConfigFlag(self::REDBOX_DIGITAL_LINKEDIN_IS_REQUIRED);
    }

    public function getMaxLength()
    {
        return (int) Mage::getStoreConfig(self::REDBOX_DIGITAL_LINKEDIN_MAXLENGTH);
    }

}